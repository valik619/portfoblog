<?php

return [
    'ABOUT' => 'About',
    'CONTACTS' => 'Contacts',
    'BLOG' => 'Blog',
    'WELCOME' => 'Hello! I\'m Valentin Sayik, a back-end developer from Ukraine, currently working at Find Out as the Software Engineer.',
    'SKILLS' => 'Skills',
    'EXAMPLES' => 'Examples',
    'CONTACT' => 'Contact',
    'NAME' => 'Valentin Sayik',
    'LAST_WORKS' => 'Examples',
    'INFO' => 'Information',
    'MORE' => 'More',
];

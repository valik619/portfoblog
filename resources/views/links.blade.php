<li><a href="{{ Linguist::url('about') }}">{{ trans('links.ABOUT') }}</a></li>

<li><a href="{{ Linguist::url('contacts') }}">
        {{ trans('links.CONTACTS') }}
    </a>
</li>

<li><a href="{{ Linguist::url('blog') }}">{{ trans('links.BLOG') }}</a></li>


@foreach($app->config->get('app.locales') as $lang)

    <?php
    if ($lang == App::getLocale()) {
        continue;
    }
    ?>

    <li>
        <a href="{{ url($lang) }}">
            <img src="{{ asset('/images/icons/'.$lang.'.png') }}" style="vertical-align: middle; height: 24px;" />
        </a>
    </li>
@endforeach
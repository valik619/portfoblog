@extends('layouts.main')

@section('title', 'About Me {{  $id }}')
@section('pageH1', 'Web-Developer')
@section('pageDesc', 'Hello! At this page you may find my contacts.')

@section('top-buttons')
    <a href="#" id="download-button" class="btn-large waves-effect waves-light blue lighten-1">TW</a>
    <a href="#" id="download-button" class="btn-large waves-effect waves-light blue darken-2">VK</a>
    <a href="#" id="download-button" class="btn-large waves-effect waves-light blue darken-3">FB</a>
@stop

@section('[ah')
    <p>This is prepended to the master sidebar.</p>

    @parent

    <p>This is appended to the master sidebar.</p>
@stop

@section('content')
    <p>This is my body content.</p>
@stop
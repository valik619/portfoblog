@extends('layouts.main')

@section('title', 'MainPage')
@section('pageH1', 'Web-Developer')
@section('pageDesc', trans('links.WELCOME'))

@section('top-buttons')
    <a href="#examples" class="btn-large waves-effect waves-light blue darken-2">{{ trans('links.EXAMPLES') }}</a>
    <a href="#skills" class="btn-large waves-effect waves-light green darken-2">{{ trans('links.SKILLS') }}</a>
    <a href="#contact" class="btn-large waves-effect waves-light red darken-2">{{ trans('links.CONTACT') }}</a>
@stop

@section('content')
    <div id="examples" class="container scrollspy">
        <div class="row">

            <h2>{{ trans('links.LAST_WORKS') }}</h2>

            @foreach($model as $arr)

                <?php /* TODO default locale */ $arr->translate = $arr->translate->where('locale', Linguist::workingLocale())->first() ?>

                <div class="col l4 m6 s12">
                    <div class="card">
                        <div class="card-image darken-5">
                            <img class="opacity-8"
                                 src="{{ ImageManager::getImagePath(public_path() . '/' . $arr->screen->filename, 480, 260, 'crop') }}">
                            <span class="card-title page-desc">{{ $arr->translate->name }}</span>
                        </div>
                        <div class="card-content">
                            <p>{{ $arr->translate->description }}</p>
                        </div>
                        <div class="card-action">
                            <a href="#" class="blue-text text-darken-1">{{ trans('links.INFO') }}</a>
                        </div>
                    </div>
                </div>

            @endforeach
            <div class="clearfix"></div>

            <div class="center-align center-block">
                <a href="#" class="btn-large waves-effect waves-light blue darken-3">{{ trans('links.MORE') }}</a>
            </div>


        </div>
    </div>

    <div id="skills" class="container scrollspy">
        <div class="row">

            <h2>{{ trans('links.SKILLS') }}</h2>

            <div class="col l12">
                php Yii2 / Laravel 5 mysql ...
            </div>
        </div>
    </div>
@stop
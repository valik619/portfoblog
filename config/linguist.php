<?php

return [

    /*
     * i18n locale slugs
     */
    'locales' => ['uk', 'ru', 'en'],

    /*
     * Hide i18n slug for default locale
     */
    'hide_default' => true,

    /*
     * Default i18n locale slug
     */
    'default' => 'uk'
];
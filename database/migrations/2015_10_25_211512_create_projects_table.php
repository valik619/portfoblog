<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Meta Information
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('price');
            $table->integer('time');
            $table->timestamps();
        });
        //Translated Info
        Schema::create('projects_translate', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->string('name');
            $table->text('description');
            $table->string('locale', 2); #uk, en, ru...
            $table->timestamps();
        });
        //Screenshots
        Schema::create('projects_screen', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->string('filename');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('projects');
        Schema::drop('projects_translate');
        Schema::drop('projects_screen');
    }
}

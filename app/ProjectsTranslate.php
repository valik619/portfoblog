<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectsTranslate extends Model
{
    protected $table = 'projects_translate';

    public function translate(){
        return $this->belongsTo('Projects');
    }

}

<?php

/**
 * @class
 */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

#Route::controller('about','IndexController');

Route::get('/', [
    'as' => 'Home', 'uses' => 'Portfolio\IndexController@index'
]);


Route::get('about', [
    'as' => 'About', 'uses' => 'Portfolio\IndexController@about'
]);

Route::get('contacts', [
    'as' => 'Contacts', 'uses' => 'Portfolio\IndexController@contacts'
]);

Route::group(['prefix' => 'blog'], function()
{
    Route::get('/', [
        'as' => 'Blog MainPage', 'uses' => 'Blog\IndexController@index'
    ]);

    Route::get('post/{id}', [
        'as' => 'Article', 'uses' => 'Blog\IndexController@post'
    ]);

});




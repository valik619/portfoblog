<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{

    public function index()
    {
        return view('portfolio.about');
    }

    public function post($id)
    {
        return view('portfolio.about', [
            'id' => $id
        ]);
    }

}

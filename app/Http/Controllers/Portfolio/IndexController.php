<?php

namespace App\Http\Controllers\Portfolio;

use App\Http\Controllers\Controller;
use App\Projects;
#use App\ProjectsTranslate;

class IndexController extends Controller
{

    public function index()
    {
        $model = Projects::all()->take(3)->sortByDesc('created_at');

        return view('portfolio.index',[
            'model' => $model,
        ]);
    }

    public function about()
    {
        return view('portfolio.about');
    }

    public function contacts()
    {
       # return view('portfolio.contacts');
    }
}

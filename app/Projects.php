<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    protected $table = 'projects';

    public function translate()
    {
        return $this->hasMany('App\ProjectsTranslate', 'project_id', 'id');
    }
    public function screen()
    {
        return $this->hasOne('App\ProjectsScreen', 'project_id', 'id');
    }
}
